<!-- Lead Triage wrapper -->
<div id="wrapper" class="lead-triage">

  <!-- header -->
  <div id="lt-header">
    <a href="<?php print base_path() . 'lead_triage'; ?>">
      <h1>Lead Triage</h1>
    </a>
  </div>

  <!-- sidebar -->
  <div id="lt-sidebar">

    <!-- nav -->
    <nav id="lt-nav-cont" role="navigation">
      <ul class="lt-nav">
        <?php print render($page['sidebar_first']); ?>
      </ul>
    </nav>

  </div>

  <!-- content area -->
  <div id="lt-page-wrapper">
    <?php print render($page['content']); ?>
  </div>

</div>
