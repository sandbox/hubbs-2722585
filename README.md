# Lead Triage
-------------
Module that gives administrators an interface to create and assign leads to
sales representatives. Leads get committed to HubSpot and placed into the sales
representatives account.


# Initial Setup
---------------
1) Install and activate modules
2) Install Jquery Update module and set admin area Jquery to 1.10 (or greater)
3) Set HubSpot ID
4) Set access permissions (note: A new role could be created for sales reps)
5) Make sure at least one user is setup with the sales rep permission role


# TO FIX
--------
- Change HubSpot Integration module - Currently the API key is hardcoded in.
  Need to modify so that user enters their own info.
  * Acro internal
    See 'Barcodes Talk' HubSpot integration module as this was done there.
- Update dependencies to include Jquery Update
- Leads overview - 'Collapse/Expand' button doesn't do anything when no sales
  reps in system. Button should be visually 'disabled'
- Number of deleted leads is shown in Dashboard but no way to see what leads
  they were. Would be nice to see them with ability to assign/commit or
  permanently delete.
- Dashboard should have a way to commit uncommitted leads.
- Lead 'priority' status doesn't have any effect in HubSpot. Should it?
- * Acro internal
    Module doesn't integrate well with Acro Corp Site. Dashboard shows other
    random page elements. This doesn't happen in a fresh D7 install so may or
    may not be an issue with this module.
