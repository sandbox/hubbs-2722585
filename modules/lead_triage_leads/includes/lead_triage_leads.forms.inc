<?php
/**
 * @file
 * Contains the add lead page form and the block contact form settings form.
 */

/**
 * Page callback:  settings
 *
 * @see form_test_menu()
 */
function lead_triage_leads_contact_form_settings_form($form, &$form_state) {
  $form['overview_page'] = array(
    '#type' => 'container'
  );

  $form['overview_page']['label'] = array(
    '#markup' => '<h3>' . t('Overview Page') . '</h3>'
  );

  $form['overview_page']['lead_triage_leads_default_delete_action'] = array(
    '#type' => 'select',
    '#title' => t('Default Delete Action'),
    '#default_value' => variable_get('lead_triage_leads_default_delete_action', 'mark_delete'),
    '#options' => array(
      'mark_delete' => 'Mark as deleted',
      'permanently_delete' => 'Permanently Delete'
    ),
    '#description' => t('Select what action should occur when the lead delete button is clicked. A confirm dialog will be presented before any deletion occures.')
  );

  $form['contact_form'] = array(
    '#type' => 'container'
  );

  $form['contact_form']['label'] = array(
    '#markup' => '<h3>' . t('Contact Form') . '</h3>'
  );

  $form['contact_form']['lead_triage_leads_name_field'] = array(
    '#type' => 'radios',
    '#title' => t('Name field type'),
    '#default_value' => variable_get('lead_triage_leads_name_field', 'name'),
    '#options' => array('name' => 'Just a Name field', 'firstname_lastname' => 'Firstname and Lastname fields'),
    '#description' => t('Should the form contain a single name field, or a firstname and a lastname field?')
  );

  return system_settings_form($form);
}
