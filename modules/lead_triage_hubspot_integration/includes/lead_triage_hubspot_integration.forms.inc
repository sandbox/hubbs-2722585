<?php
function lead_triage_hubspot_integration_settings_form() {
  $form['lead_triage_hubspot_integration_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('HubSpot Client ID'),
    '#default_value' => variable_get('lead_triage_hubspot_integration_client_id'),
    '#description' => t('The Client ID of your HubSpot App. This can be found by logging into HubSpot and going to your applications information page. To setup a new HubSpot App, go to the <a href="@hubspot-developer-site" target="_blank" title="HubSpot developer site">HubSpot developer site</a> and create a developer account.',
      array(
        '@hubspot-developer-site' => 'http://developers.hubspot.com/docs/overview'
      )
    )
  );

  $form['lead_triage_hubspot_integration_hub_id'] = array(
    '#type' => 'textfield',
    '#title' => t('HubSpot Hub ID'),
    '#default_value' => variable_get('lead_triage_hubspot_integration_hub_id'),
    '#description' => t('The HubSpot Hub ID of the account that you want to integrate with. Can be found by logging into HubSpot and looking at the number in the top right.')
  );

  return system_settings_form($form);
}
