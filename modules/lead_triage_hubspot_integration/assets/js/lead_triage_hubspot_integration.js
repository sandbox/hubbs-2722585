Drupal.behaviors.lead_triage_hubspot_integration = {
  'attach': function (context) {
    $(document).ready(function (event) {

      $('#commit-leads-button').click(function () {
        commitAssignedLeads();
      });

      // Commits all the currently assigned leads in the database to hubspot.
      function commitAssignedLeads() {
        var commit_leads_button = $('#commit-leads-button');
        var module_base_path = commit_leads_button.data('module-base-path');
        var imgTag = '<img id="commit-loading-gif" alt="loading" src="' + Drupal.settings.basePath + module_base_path + '/templates/assets/images/ajax-loader.gif">';
        $(imgTag).insertBefore(commit_leads_button);
        commit_leads_button.hide();

        $.ajax({
          url: Drupal.settings.basePath + 'lead_triage_hubspot_integration/commit_assigned_leads',
          dataType: 'text',
          success: function (data) {
            var returnObj = $.parseJSON(data.trim());

            if (returnObj.status === 'success') {
              $('.sales-rep-lead-list').each(function () {
                $(this).find('[data-lead-id]').each(function () {
                  $(this).remove();
                });
                $(this).not(':has(.no-assigned-leads)').append('<li class="list-group-item no-assigned-leads">' + Drupal.t('No Assigned Leads') + '</li>');
              });
              $('.priority-indicator').text('0');
              lead_triage_displayAlertMsg('success', Drupal.t('Leads created on HubSpot successfully.'));
            }
            else if (returnObj.status === 'no_leads_to_commit') {
              lead_triage_displayAlertMsg('info', Drupal.t('There are no leads to send to HubSpot.'));
            }
            else if (returnObj.status === 'get_oauth_token') {
              window.location.replace(returnObj.url);
            }
            else {
              if (returnObj.leads) {
                for (var i = 0; i < returnObj.leads.length; i++) {
                  $('.sales-rep-lead-list').find('[data-lead-id="' + returnObj.leads[i].id + '"]').remove();
                  var prioritySpan = $('#' + returnObj.leads[i].priority + '-' + returnObj.leads[i].uid);
                  var count = prioritySpan.text();
                  prioritySpan.text(count - 1);
                }
                $('.sales-rep-lead-list').not(':has([data-lead-id])').append('<li class="list-group-item no-assigned-leads">' + Drupal.t('No Assigned Leads') + '</li>');
              }
              lead_triage_displayAlertMsg('error', Drupal.t(returnObj.message));
            }
            $('#commit-loading-gif').remove();
            commit_leads_button.show();
          },
          error: function (jqXHR, textStatus, errorThrown) {
            lead_triage_displayAlertMsg('error', Drupal.t('Something went wrong when trying to send the leads to HubSpot.'));
            $('#commit-loading-gif').remove();
            commit_leads_button.show();
          }
        });
      }
    });
  }
}