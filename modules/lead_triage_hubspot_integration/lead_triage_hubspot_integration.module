<?php

// The base url for api calls
define("LEAD_TRIAGE_HUBSPOT_API_BASE_URL", "https://api.hubapi.com");

define("LEAD_TRIAGE_HUBSPOT_APP_BASE_URL", "https://app.hubspot.com");

// The endpoint for dealing with HubSpot owners
define("LEAD_TRIAGE_HUBSPOT_OWNERS_ENDPOINT", "/owners/v2/owners");

// The endpoint for dealing with HubSpot contacts
define("LEAD_TRIAGE_HUBSPOT_CONTACTS_ENDPOINT", "/contacts/v1/contact");

// The endpoint for requesting an access token.
define("LEAD_TRIAGE_HUBSPOT_AUTHENTICATE_ENDPOINT", "/auth/authenticate");

/**
 * Implements hook_help().
 *
 * Displays help and module information.
 *
 * @param $path
 *  Path of the site to display help
 * @param $arg
 *  Array that holds the current path returned by the arg() function.
 * @return string
 *  An HTML string which describes the option.
 */
function lead_triage_hubspot_integration_help($path, $arg) {
  switch ($path) {
    case "admin/help#lead_triage_hubspot_integration":
      return '<p>' . t("HubSpot integration for the Lead Triage Leads module") . '</p>';
      break;
  }
}

/**
 * Implements hook_menu
 *
 * @TODO: JSON returns can use 'delivery callback' in menu hook to product return
 * see: https://api.drupal.org/api/drupal/includes!common.inc/function/drupal_json_output/7
 */
function lead_triage_hubspot_integration_menu() {
  $items = array();

  $items['lead_triage_hubspot_integration/commit_assigned_leads'] = array(
//    'access callback' => TRUE,
    'page callback' => 'lead_triage_hubspot_integration_commit_assigned_leads_ajax',
    'type' => MENU_CALLBACK,
    'access callback' => 'lead_triage_check_user_has_crm_role',
  );

  $items['lead_triage_hubspot_integration/oauth_token'] = array(
//    'access callback' => TRUE,
    'page callback' => 'lead_triage_hubspot_integration_oauth_token',
    'type' => MENU_CALLBACK,
    'access callback' => 'lead_triage_check_user_has_crm_role',
  );

  $items['admin/config/lead_triage/lead_triage_hubspot_integration'] = array(
    'title' => 'Lead Triage HubSpot Integration',
    'description' => 'Configuration for the Lead Triage HubSpot Integration',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('lead_triage_hubspot_integration_settings_form'),
    'file' => 'includes/lead_triage_hubspot_integration.forms.inc',
    'access callback' => 'lead_triage_check_user_has_admin_role',
    'type' => MENU_NORMAL_ITEM
  );

  return $items;
}

/**
 * Implements hook_init().
 *
 * Attaches the necessary javascript for attaching and processing a
 * "Connect to QuickBooks" button.
 */
function lead_triage_hubspot_integration_init() {
  $path = request_uri();

  if ($path == '/lead_triage_leads/overview') {
    $path = drupal_get_path('module', 'lead_triage_hubspot_integration');

    drupal_add_js($path . '/assets/js/lead_triage_hubspot_integration.js', array('scope' => 'footer'));
  }
}

/**
 * Implements hook_lead_triage_overview_filter_buttons_alter
 * @param $form
 */
function lead_triage_hubspot_integration_lead_triage_overview_filter_buttons_alter(&$form) {
  // Add sync to HubSpot button to overview page
  $form['hubspot_integration'] = array(
    '#type' => 'button',
    '#id' => 'commit-leads-button',
    '#value' => t('Commit Leads'),
    '#attributes' => array(
      'class' => array(
        'btn',
        'btn-primary'
      ),
      'data-module-base-path' => drupal_get_path('module', 'lead_triage')
    )
  );
}

/**
 * Saves the access and refresh tokens then redirects to the overview page.
 */
function lead_triage_hubspot_integration_oauth_token() {
  $access_token = $_GET['access_token'];
  $refresh_token = $_GET['refresh_token'];

  variable_set('lead_triage_hubspot_integration_oauth_access_token', $access_token);
  variable_set('lead_triage_hubspot_integration_oauth_refresh_token', $refresh_token);

  drupal_goto(base_path() . 'lead_triage_leads/overview');
}

/**
 * Builds the request url to get the HubSpot oauth token
 * @return string The request url to get the HubSpot oauth token.
 */
function lead_triage_hubspot_integration_get_oauth_request_url() {
  global $base_url;
  $redirect_uri = $base_url . base_path() . 'lead_triage_hubspot_integration/oauth_token';
  $scope = 'offline';
  return LEAD_TRIAGE_HUBSPOT_APP_BASE_URL . LEAD_TRIAGE_HUBSPOT_AUTHENTICATE_ENDPOINT . '?client_id=' . variable_get('lead_triage_hubspot_integration_client_id') . '&portalId=' . variable_get('lead_triage_hubspot_integration_hub_id') . '&redirect_uri=' . $redirect_uri . '&scope=' . $scope;
}

/**
 * Tries to turn all the assigned leads in the database into contacts on HubSpot. If a lead is successfully added,
 * its status is updated to COMMITTED. If a lead fails to update on HubSpot, its error response is saved and the
 * user is notified.
 */
function lead_triage_hubspot_integration_commit_assigned_leads_ajax() {
  // Get all the assigned leads from the DB.
  $leads = db_query('SELECT leads.lead_id, email, firstname, leads.name, phone, comments, assigned_leads.uid, priority, mail
                        FROM leads
                        INNER JOIN assigned_leads
                        ON leads.lead_id=assigned_leads.lead_id
                        INNER JOIN users
                        ON users.uid = assigned_leads.uid
                        WHERE leads.status = \'UNCOMMITTED\'');

  if (variable_get('lead_triage_hubspot_integration_oauth_access_token') === NULL) {
    print '{"status": "get_oauth_token", "url": "' . lead_triage_hubspot_integration_get_oauth_request_url() . '"}';
  } else {

    // Try and refresh the access token
    $endpoint = 'https://api.hubapi.com/auth/v1/refresh';
    $data = 'refresh_token=' . variable_get('lead_triage_hubspot_integration_oauth_refresh_token') .
      '&client_id=' . variable_get('lead_triage_hubspot_integration_client_id') .
      '&grant_type=refresh_token';

    $ch = @curl_init();
    @curl_setopt($ch, CURLOPT_URL, $endpoint);
    @curl_setopt($ch, CURLOPT_POST, TRUE);
    @curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
    @curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $response_json = @curl_exec($ch);
    $response = json_decode($response_json);
    $status = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
    @curl_close($ch);

    // If the refresh request succeeded
    if ($status == 200) {
      variable_set('lead_triage_hubspot_integration_oauth_access_token', $response->access_token);
      variable_set('lead_triage_hubspot_integration_oauth_refresh_token', $response->refresh_token);

      $success = TRUE;
      $leads_to_commit = TRUE;
      $error_leads = array();
      $success_leads = array();
      $owners_map = lead_triage_hubspot_integration_get_hubspot_owners();

      // For each of the assigned leads, try and create it on hubspot.
      // If success, update its status, otherwise save its error.
      if ($leads->rowCount() > 0) {
        while ($lead = $leads->fetch()) {
          $owner_id = $owners_map[$lead->mail];
          $lead_array = lead_triage_hubspot_integration_get_lead_array_for_hubspot($lead, $owner_id);
          $response = lead_triage_hubspot_integration_create_contact_on_hubspot($lead_array);
          if ($response->status == "error") {
            $success = FALSE;
            array_push($error_leads, array(
              "lead" => $lead,
              "response" => $response
            ));
          } else {
            array_push($success_leads, $lead);
            db_update('leads')
              ->fields(array('status' => 'COMMITTED'))
              ->condition('lead_id', $lead->lead_id)
              ->execute();
          }
        }
      } else {
        $leads_to_commit = FALSE;
      }

      // Everything succeeded.
      if ($success && $leads_to_commit) {
        print '{"status": "success"}';
      } // No leads to commit.
      elseif (!$leads_to_commit) {
        print '{"status": "no_leads_to_commit"}';
      } // There was one or more errors. Build and return the error message.
      else {
        $error_message = '<p>There were errors with some of the leads you tried to send to HubSpot: </p><ul>';
        foreach ($error_leads as $error_lead) {
          $error_message .= '<li>';
          if ($error_lead['response']->message == "Contact already exists") {
            $error_message .= 'A contact with the email \'' . $error_lead['lead']->email . '\'  already exists, and could not be created.';
          } elseif (preg_match('/^Email address [A-Za-z@_\-]* is invalid$/', $error_lead['response']->message) == 1) {
            $error_message .= 'The email \'' . $error_lead['lead']->email . '\' is not valid on HubSpot and the contact could not be created.';
          } else {
            $error_message .= $error_lead['response']->message;
          }
          $error_message .= '</li>';
        }
        $error_message .= '</ul>';
        print '{"status": "error", "message": "' . $error_message . '"' . (sizeof($success_leads) > 0 ? (', "leads": ' . lead_triage_hubspot_integration_build_json_leads_array($success_leads)) : '') . '}';
      }
    } else {
      print '{"status": "get_oauth_token", "url": "' . lead_triage_hubspot_integration_get_oauth_request_url() . '"}';
    }
  }
}

/**
 * Creates a json string that represents the leads that were created successfully on HubSpot.
 * @param $success_leads array An array of the leads that were created successfully on HubSpot.
 * @return string A json string representing the leads that were created.
 */
function lead_triage_hubspot_integration_build_json_leads_array($success_leads) {
  $json_array = '';
  if (sizeof($success_leads) > 0) {
    $json_array .= '[';
    for ($i = 0; $i < sizeof($success_leads) - 1; $i++) {
      $json_array .= lead_triage_hubspot_integration_build_json_lead_object($success_leads[$i]) . ',';
    }

    // Need to do this to make sure that there is no extra comma at the end of the array.
    $json_array .= lead_triage_hubspot_integration_build_json_lead_object($success_leads[sizeof($success_leads) - 1]);

    $json_array .= ']';
  }
  return $json_array;
}

/**
 * Builds a json object that represents a lead.
 * @param $lead object The lead object.
 * @return string A json string object.
 */
function lead_triage_hubspot_integration_build_json_lead_object($lead) {
  $json_object = '{';

  $json_object .= '"id": ' . $lead->lead_id . ',';

  $json_object .= '"priority": ' . '"' . ($lead->priority == NULL ? 'unassigned' : $lead->priority) . '",';
  $json_object .= '"uid": ' . $lead->uid;

  return $json_object . '}';
}

/**
 * Gets the array of the lead properties to send to HubSpot.
 * @param $lead object A lead object.
 * @param $owner_id int The id of the owner that the lead will be assigned to.
 * @return array An array representing the lead to send to HubSpot.
 */
function lead_triage_hubspot_integration_get_lead_array_for_hubspot($lead, $owner_id) {

  $arr = array(
    'properties' => array(
      array(
        'property' => 'email',
        'value' => $lead->email,
      ),
      array(
        'property' => 'firstname',
        'value' => $lead->firstname,
      ),
      array(
        'property' => 'lastname',
        'value' => $lead->name,
      ),
      array(
        'property' => 'phone',
        'value' => $lead->phone,
      ),
      array(
        "property" => "lifecyclestage",
        "value" => "lead",
      ),
      array(
        "property" => "message",
        "value" => $lead->comments,
      ),
      array(
        "property" => "hubspot_owner_id",
        "value" => $owner_id,
      ),
    )
  );
  return $arr;
}

/**
 * Queries HubSpot and returns all the contact owners and their owner ids. This is needed to assign the contact being
 * created through this module to the correct owner on HubSpot.
 * @return array
 */
function lead_triage_hubspot_integration_get_hubspot_owners() {
  $endpoint = LEAD_TRIAGE_HUBSPOT_API_BASE_URL . LEAD_TRIAGE_HUBSPOT_OWNERS_ENDPOINT . lead_triage_hubspot_integration_get_hubspot_access_token_argument();
  $response_json = lead_triage_hubspot_integration_query_hubspot($endpoint, null);

  $owners_array = json_decode($response_json);

  $owners_map = array();

  foreach ($owners_array as $owner) {
    $owners_map[$owner->email] = $owner->ownerId;
  }

  return $owners_map;
}

/**
 * Submits a query to the Hubspot api.
 * @param $endpoint String The endpoint url to send the query to.
 * @param $post_parameter String The json string post argument to send to Hubspot.
 * @return String The json string response from HubSpot.
 */
function lead_triage_hubspot_integration_query_hubspot($endpoint, $post_parameter) {
  $ch = @curl_init();

  if ($post_parameter != null) {
    @curl_setopt($ch, CURLOPT_POST, TRUE);
    @curl_setopt($ch, CURLOPT_POSTFIELDS, $post_parameter);
  }

  @curl_setopt($ch, CURLOPT_URL, $endpoint);
  @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
  @curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  $response_json = @curl_exec($ch);
  @curl_close($ch);
  return $response_json;
}

/**
 * Returns the api key argment for the url.
 * @return string The api key argument.
 */
function lead_triage_hubspot_integration_get_hubspot_api_key_argument() {
  return '?hapikey=' . variable_get('lead_triage_hubspot_integration_api_key');
}

function lead_triage_hubspot_integration_get_hubspot_access_token_argument() {
  return '?access_token=' . variable_get('lead_triage_hubspot_integration_oauth_access_token');
}

/**
 * Creates the given lead as a contact on HubSpot.
 * @param $lead_array array The array that represents the lead.
 * @return mixed The response from HubSpot.
 */
function lead_triage_hubspot_integration_create_contact_on_hubspot($lead_array) {
  $post_json = json_encode($lead_array);
//        $hapikey = '957057dc-bced-43ed-8b64-c08dc88c3974';
  $endpoint = LEAD_TRIAGE_HUBSPOT_API_BASE_URL . LEAD_TRIAGE_HUBSPOT_CONTACTS_ENDPOINT . lead_triage_hubspot_integration_get_hubspot_access_token_argument();

  $response_json = lead_triage_hubspot_integration_query_hubspot($endpoint, $post_json);

  return json_decode($response_json);
}
