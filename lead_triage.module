<?php
/**
 * @file
 * Defines basic functionality common to all parts of Lead Triage.
 */

/**
 * Implements hook_help().
 *
 * Displays help and module information.
 *
 * @param path
 *   Which path of the site we're using to display help
 * @param arg
 *   Array that holds the current path as returned from arg() function
 */
function lead_triage_help($path, $arg) {
  switch ($path) {
    case "admin/help#lead_triage":
      return '<p>' . t("Establishes basic functionality for all parts of Lead Triage") . '</p>';
      break;
  }
}

/**
 * Implements hook_menu().
 */
function lead_triage_menu() {
  $items = array();

  // Menu items that are basically just menu blocks.
  $items['admin/lead_triage'] = array(
    'title' => 'Lead Triage',
    'description' => 'Lead Triage',
    'weight' => -8,
    'page callback' => 'lead_triage_landing_page',
    'access callback' => 'lead_triage_check_user_has_admin_role',
    'file' => 'includes/lead_triage.pages.inc',
  );
  $items['lead_triage'] = array(
    'title' => 'Lead Triage',
    'description' => 'Lead Triage',
    'weight' => -8,
    'type' => MENU_CALLBACK,
    'page callback' => 'lead_triage_landing_page',
    'access callback' => 'lead_triage_check_user_has_crm_role',
    'file' => 'includes/lead_triage.pages.inc',
  );
  $items['lead_triage/default'] = array(
    'title' => 'Lead Triage',
    'description' => 'Lead Triage',
    'weight' => -8,
    'type' => MENU_CALLBACK,
    'page callback' => 'lead_triage_default',
    'access callback' => 'lead_triage_check_user_has_crm_role',
    'file' => 'includes/lead_triage.pages.inc',
  );
  $items['admin/structure/lead_triage'] = array(
    'title' => 'Lead Triage',
    'description' => 'Administer Lead Triage items, such as team members, contacts, etc.',
    'page callback' => 'system_admin_menu_block_page',
    'access callback' => 'lead_triage_check_user_has_admin_role',
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );
  $items['admin/config/lead_triage'] = array(
    'title' => 'Lead Triage',
    'description' => 'Settings for Lead Triage modules.',
    'position' => 'right',
    'weight' => -10,
    'page callback' => 'system_admin_menu_block_page',
    'access callback' => 'lead_triage_check_user_has_admin_role',
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );
  $items['admin/config/lead_triage/crm'] = array(
    'title' => 'Lead Triage settings',
    'description' => 'Configure Lead Triage.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('lead_triage_settings'),
    'access callback' => 'lead_triage_check_user_has_admin_role',
    'file' => 'includes/lead_triage.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function lead_triage_permission() {
  return array(
    'administer lead_triage' => array(
      'title' => t('Administer Lead Triage'),
      'description' => t('Perform administration tasks for my Lead Triage.'),
    ),
    'sales rep' => array(
      'title' => t('Sales Representative'),
      'description' => t('A person who is part of the sales division.'),
    ),
  );
}

/**
 * Implements hook_theme().
 */
function lead_triage_theme() {
  return array(
    'page__lead_triage' => array(
      'variables' => array('page' => NULL),
      'preprocess functions' => array('lead_triage_page'),
    ),
    'lead_triage_home' => array(
      'base hook' => 'page__lead_triage',
      'path' => drupal_get_path('module', 'lead_triage') . '/templates',
      'template' => 'main--lead_triage',
    ),
  );
}

/**
 * These are things that will appear on every page.
 *
 * @param $variables
 */
function lead_triage_page(&$variables) {
  $page = $variables['page'];
  $module_path = drupal_get_path('module', 'lead_triage');
  $css_path = $module_path . '/templates/assets/css/';
  $js_path = $module_path . '/templates/assets/js/';

  // css for header
  drupal_add_css($css_path . 'lead_triage_reset.css', array('group' => CSS_THEME));
  drupal_add_css($css_path . 'metismenu.min.css', array('group' => CSS_THEME));
  drupal_add_css($css_path . 'timeline.css', array('group' => CSS_THEME));
  drupal_add_css($css_path . 'font-awesome.min.css', array('group' => CSS_THEME));
  drupal_add_css($css_path . 'lead_triage.css', array('group' => CSS_THEME));

  // js for header
  drupal_add_js('var $ = jQuery;', 'inline', array('group' => JS_THEME));
  drupal_add_js($js_path . 'bootstrap.min.js', array('group' => JS_THEME));
  drupal_add_js($js_path . 'sb-admin-2.js', array('group' => JS_THEME));
  drupal_add_js($js_path . 'metismenu.min.js', array('group' => JS_THEME));

  // js for footer
  drupal_add_js($js_path . 'lead_triage.js', array('scope' => 'footer'));

  // Add items to sidebar
  $items = array();

  if (!$items) {
    // get list of triage items for the sidebar
    $items = module_invoke_all('lead_triage_items_info');

    // allow modules to alter the list of triage pages
    drupal_alter('lead_triage_items_info', $items);
  }

  if (lead_triage_check_user_has_admin_role()) {
    $items['settings'] = array(
      'path' => base_path() . 'admin/config/lead_triage',
      'title' => t('Settings'),
      'icon' => 'fa-cog',
      'permission' => 'administer lead_triage'
    );
  }

  $markup = '';
  foreach ($items as $item) {
    $markup .= '<li><a href="' . $item['path'] . '">';
    $markup .= '<i class="fa ' . $item['icon'] . ' fa-fw"></i>' . $item['title'];

    if (array_key_exists('child_pages', $item)) {
      $markup .= '<span class="fa arrow"></span></a>'
        . '<ul class="nav nav-second-level">';

      foreach ($item['child_pages'] as $child_item) {
        $markup .= '<li>'
          . l($child_item['title'], $child_item['path'])
          . '</li >';
      }

      $markup .= '</ul>';
    }
    else {
      $markup .= '</a>';
    }

    $markup .= '</li>';
  }

  $page['sidebar_first']['links'] = array(
    '#type' => 'markup',
    '#markup' => $markup,
  );

  $variables['page'] = $page;
}

/**
 * Implements hook_admin_paths().
 */
function lead_triage_admin_paths() {
  // Set Lead Triage admin paths based on configuration.
  if (variable_get('lead_triage_admin_path', TRUE)) {
    $paths = array(
      'lead_triage*' => TRUE,
      'lead_triage/*' => TRUE,
    );
    return $paths;
  }
}

function lead_triage_get_message_markup() {
  $messages = drupal_get_messages();
  $markup = '<div class="messages">';
  if (isset($messages['error']) && $messages['error']) {
    $markup .= '<div class="alert alert-danger">';
    foreach ($messages['error'] as $message) {
      $markup .= t($message) . '<br>';
    }
    $markup .= '</div>';
  }
  if (isset($messages['status']) && $messages['status']) {
    $markup .= '<div class="alert alert-success">';
    foreach ($messages['status'] as $message) {
      $markup .= t($message) . '<br>';
    }
    $markup .= '</div>';
  }
  $markup .= '</div>';
  return $markup;
}

function lead_triage_check_user_has_admin_role() {
  return user_access('administer lead_triage');
}

function lead_triage_check_user_has_sales_rep_role() {
  global $user;
  return user_access('sales rep') && $user->uid != 1;
}

function lead_triage_check_user_has_crm_role() {
  return user_access('sales rep') || user_access('administer lead_triage');
}

/**
 * Generate an array of users with the sales rep role.
 *
 * @return Array
 *  An array of users with the sales rep role.
 */
function lead_triage_get_sales_reps($sales_role = 'sales rep') {
  // Query for user ids.
  $query = db_select('users', 'u');
  $query->fields('u');
  $query->leftJoin('users_roles', 'r', 'u.uid = r.uid');
  $query->leftJoin('role_permission', 'p', 'r.rid = p.rid');
  $and = db_and()
    ->condition('u.uid', 0, '!=')
    ->condition('u.uid', 1, '!=')
    ->condition('p.permission', $sales_role, '=');
  $query
    ->condition($and);
  $results = $query->execute();
  $results = $results->fetchAll();

  if (empty($results)) {
    return array();
  }

  // Make array of ids from query returned structure.
  $user_ids = array();
  foreach($results as $user){
    if (!isset($user->uid) || empty($user->uid)) {
      // Not a valid user id to load.
      continue;
    }

    array_push($user_ids, $user->uid);
  }

  // Load full user objects.
  $users = user_load_multiple($user_ids);

  return $users;
}
