<?php

function lead_triage_landing_page() {
    if (module_exists('lead_triage_dashboard')) {
        drupal_goto($path = "/lead_triage_dashboard");
    } elseif (module_exists('lead_triage_leads')) {
        drupal_goto($path = "/lead_triage_leads/overview");
    } else {
        drupal_goto($path = "/lead_triage/default");
    }
}

function lead_triage_default() {
    $page['content'] = array(
        '#markup' => '<h2>There are no modules installed.</h2>'
    );
    return theme('lead_triage_home', array('page' => $page));
}
