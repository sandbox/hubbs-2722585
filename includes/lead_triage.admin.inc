<?php

/**
 * Settings form for Lead Triage.
 *
 * @return array
 *   System settings form array.
 */
function lead_triage_settings() {
  $form['intro'] = array(
    '#type' => 'item',
    '#markup' => t('Lead Triage settings.'),
  );
  $form['lead_triage_admin_path'] = array(
    '#title' => t('Treat Lead Triage paths as administrative'),
    '#type' => 'checkbox',
    '#description' => t('This is used by other modules to, for example, use the admin theme on Lead Triage paths.'),
    '#default_value' => variable_get('lead_triage_admin_path', TRUE),
  );

  // Allow other modules to inject their own settings.
  $form += module_invoke_all('lead_triage_settings');
  return system_settings_form($form);
}
